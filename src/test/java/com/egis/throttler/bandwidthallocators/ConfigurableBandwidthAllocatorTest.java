package com.egis.throttler.bandwidthallocators;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static com.egis.throttler.bandwidthallocators.ConfigurableBandwidthAllocator.BandwidthDefinition.from;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.testng.Assert.assertEquals;

import org.joda.time.DateTimeUtils;
import org.joda.time.LocalTime;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.egis.throttler.bandwidthallocators.ConfigurableBandwidthAllocator;

public class ConfigurableBandwidthAllocatorTest {
  
  @Test
  public void should_give_correct_allocation() {
    LocalTime localTimeNow = LocalTime.now();
    String now = localTimeNow.toString();
    String in2Seconds = localTimeNow.plusSeconds(2).toString();
    String in4Seconds = localTimeNow.plusSeconds(4).toString();
    
    ConfigurableBandwidthAllocator allocator = new ConfigurableBandwidthAllocator.Builder()
      .resetPeriod(1, SECONDS)
      .addBandwidthDefinition(from(now).to(in2Seconds).allocate(64))
      .addBandwidthDefinition(from(in2Seconds).to(in4Seconds).allocate(128))
      .build();

    setTimeTo(now);
    assertEquals(allocator.getAvailableBandwidthInBytes(), 64);
    
    setTimeTo(in2Seconds);
    assertEquals(allocator.getAvailableBandwidthInBytes(), 128);
  }
  
  @AfterMethod
  public void afterMethod() {
    DateTimeUtils.setCurrentMillisSystem();
  }
  
  private void setTimeTo(String time) {
    DateTimeUtils.setCurrentMillisFixed(LocalTime.parse(time).toDateTimeToday().getMillis());
  }
}
