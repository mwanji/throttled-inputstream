package com.egis.throttler.bandwidthallocators;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.testng.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.egis.throttler.bandwidthallocators.HourlyBandwidthAllocator;

public class HourlyBandwidthAllocatorTest {
  
  private final HourlyBandwidthAllocator bandwidthAllocator = new HourlyBandwidthAllocator();
  
  @Test
  public void should_allow_64_kbps_from_8_to_17() {
    DateTime _8_00 = new DateTime(2014, 8, 11, 8, 0, 0, 0);
    setTimeTo(_8_00);
    
    int bandwidth = bandwidthAllocator.getAvailableBandwidthInBytes();
    
    assertEquals(bandwidth, 64000);
  }

  @Test
  public void should_allow_1_mbps_from_17_to_midnight() {
    DateTime _18_00 = new DateTime(2014, 8, 11, 18, 0, 0, 0);
    setTimeTo(_18_00);
    
    int bandwidth = bandwidthAllocator.getAvailableBandwidthInBytes();
    
    assertEquals(bandwidth, 1_000_000);
  }

  @Test
  public void should_allow_unlimited_from_midnight_to_8() {
    DateTime _1_00 = new DateTime(2014, 8, 11, 1, 0, 0, 0);
    setTimeTo(_1_00);
    
    int bandwidth = bandwidthAllocator.getAvailableBandwidthInBytes();
    
    assertEquals(bandwidth, Integer.MAX_VALUE);
  }
  
  @AfterMethod(alwaysRun=true)
  public void afterMethod() {
    DateTimeUtils.setCurrentMillisSystem();
  }
  
  @AfterClass(alwaysRun=true)
  public void afterClass() {
    bandwidthAllocator.shutdown();
  }
  
  private void setTimeTo(DateTime instant) {
    DateTimeUtils.setCurrentMillisFixed(instant.getMillis());
  }

}
