package com.egis.throttler.sharingstrategies;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.egis.throttler.StreamBucket;
import com.egis.throttler.sharingstrategies.FifoSharingStrategy;

public class FifoSharingStrategyTest {

  @Test
  public void should_allocate_all_available_bytes_to_oldest_stream_bucket() {
    FifoSharingStrategy sharingStrategy = new FifoSharingStrategy(64);
    
    StreamBucket streamBucket1 = mock(StreamBucket.class);
    StreamBucket streamBucket2 = mock(StreamBucket.class);
    
    sharingStrategy.add(streamBucket1);
    sharingStrategy.add(streamBucket2);
    
    int bandwidth = sharingStrategy.allocateBytes(64);
    
    assertEquals(bandwidth, 64);
    verify(streamBucket1).refill(64);
    verify(streamBucket2, never()).refill(anyInt());
  }

  @Test
  public void should_allocate_available_bytes_to_oldest_stream_buckets_first() {
    FifoSharingStrategy sharingStrategy = new FifoSharingStrategy(64);
    
    StreamBucket streamBucket1 = mock(StreamBucket.class);
    StreamBucket streamBucket2 = mock(StreamBucket.class);
    StreamBucket streamBucket3 = mock(StreamBucket.class);
    StreamBucket streamBucket4 = mock(StreamBucket.class);
    
    sharingStrategy.add(streamBucket1);
    sharingStrategy.add(streamBucket2);
    sharingStrategy.add(streamBucket3);
    sharingStrategy.add(streamBucket4);
    
    int bandwidth = sharingStrategy.allocateBytes(130);
    
    assertEquals(bandwidth, 130);
    verify(streamBucket1).refill(64);
    verify(streamBucket2).refill(64);
    verify(streamBucket3).refill(2);
    verify(streamBucket4, never()).refill(anyInt());
  }
}
