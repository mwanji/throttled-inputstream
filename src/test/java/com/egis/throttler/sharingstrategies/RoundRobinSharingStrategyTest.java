package com.egis.throttler.sharingstrategies;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static com.egis.throttler.testutils.Fixtures.streamBucket;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import com.egis.throttler.StreamBucket;
import com.egis.throttler.sharingstrategies.RoundRobinSharingStrategy;

public class RoundRobinSharingStrategyTest {
  
  
  @Test
  public void should_divide_bandwidth_between_streams() {
    RoundRobinSharingStrategy sharingStrategy = new RoundRobinSharingStrategy(Integer.MAX_VALUE);
    int maxBytes = 64_000;
    
    StreamBucket streamBucket1 = streamBucket();
    StreamBucket streamBucket2 = streamBucket();
    StreamBucket streamBucket3 = streamBucket();
    StreamBucket streamBucket4 = streamBucket();
    
    sharingStrategy.add(streamBucket1);
    sharingStrategy.add(streamBucket2);
    
    int bandwidth = sharingStrategy.allocateBytes(maxBytes);
    
    verify(streamBucket1).refill(32_000);
    verify(streamBucket2).refill(32_000);
    assertEquals(bandwidth, 64_000);
    
    sharingStrategy.add(streamBucket3);
    sharingStrategy.add(streamBucket4);
    
    bandwidth = sharingStrategy.allocateBytes(maxBytes);

    verify(streamBucket1).refill(16_000);
    verify(streamBucket2).refill(16_000);
    verify(streamBucket1).refill(16_000);
    verify(streamBucket2).refill(16_000);
    assertEquals(bandwidth, 64_000);
  }
  
  @Test
  public void should_respect_bucket_depth() {
    RoundRobinSharingStrategy sharingStrategy = new RoundRobinSharingStrategy(32_000);

    StreamBucket streamBucket1 = streamBucket();
    StreamBucket streamBucket2 = streamBucket();
    
    sharingStrategy.add(streamBucket1);
    sharingStrategy.add(streamBucket2);
    
    int bandwidth = sharingStrategy.allocateBytes(128_000);

    verify(streamBucket1).refill(32_000);
    verify(streamBucket2).refill(32_000);
    assertEquals(bandwidth, 64_000);
  }
}
