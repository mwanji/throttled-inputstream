package com.egis.throttler.testutils;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matcher;

import com.egis.throttler.ParentBucket;
import com.egis.throttler.StreamBucket;
import com.egis.throttler.bandwidthallocators.BandwidthAllocator;

public class Fixtures {
  private static final Random RANDOM = new Random();

  public static final StreamBucket streamBucket() {
    StreamBucket streamBucket = mock(StreamBucket.class);
    
    return streamBucket;
  }

  public static BandwidthAllocator fixedBandwidthAllocator(int bandwidth) {
    return new FixedBandwidthAllocator(bandwidth);
  }
  
  public static ByteArrayInputStream randomInputStream(int length) {
    byte[] bytes = new byte[length];
    RANDOM.nextBytes(bytes);
    
    return new ByteArrayInputStream(bytes);
  }
  
  public static Matcher<Long> between(long min, long max) {
    return both(greaterThan(min)).and(lessThan(max));
  }

  private static class FixedBandwidthAllocator implements BandwidthAllocator {
    private final int bandwidth;
    private final ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(5);

    private int getAvailableBandwidthInBytes() {
      return bandwidth;
    }
    
    @Override
    public void register(final ParentBucket bucket) {
      threadPool.scheduleAtFixedRate(new Runnable() {
        @Override
        public void run() {
          bucket.refill(getAvailableBandwidthInBytes());
          bucket.refillStreamBuckets();
        }
      }, 0, 1, TimeUnit.SECONDS);
    }
    
    @Override
    public void shutdown() {
      threadPool.shutdown();
    }
    
    private FixedBandwidthAllocator(int bandwidth) {
      this.bandwidth = bandwidth;
    }
  }
  
  private Fixtures() {}
}
