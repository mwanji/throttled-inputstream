package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static com.egis.throttler.bandwidthallocators.ConfigurableBandwidthAllocator.BandwidthDefinition.from;
import static com.egis.throttler.testutils.Fixtures.between;
import static com.egis.throttler.testutils.Fixtures.fixedBandwidthAllocator;
import static com.egis.throttler.testutils.Fixtures.randomInputStream;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.testng.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.joda.time.LocalTime;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.egis.throttler.bandwidthallocators.BandwidthAllocator;
import com.egis.throttler.bandwidthallocators.ConfigurableBandwidthAllocator;
import com.egis.throttler.sharingstrategies.RoundRobinSharingStrategy;
import com.egis.throttler.sharingstrategies.SharingStrategy;
import com.google.common.io.ByteStreams;

public class ThrottledInputStreamTest {
  
  private static final int _64_KBPS = 64 * 1024;
  
  private final ExecutorService executor = Executors.newFixedThreadPool(5);
  private final SharingStrategy UNLIMITED_SHARING_STRATEGY = new RoundRobinSharingStrategy(MAX_VALUE);
  private BandwidthAllocator allocator;

  @Test
  public void should_read_file() throws Exception {
    byte[] icon = null;
    String iconFile = getClass().getResource("/com/egis/throttler/140kb.xpm").getFile();
    try (FileInputStream normalInputStream = new FileInputStream(iconFile);
        ByteArrayOutputStream normalOutputStream = new ByteArrayOutputStream();) {
      ByteStreams.copy(normalInputStream, normalOutputStream);
      
      icon = normalOutputStream.toByteArray();
    }

    allocator = fixedBandwidthAllocator(100_000);
    try (ThrottledInputStream throttledInputStream = new ThrottledInputStream(new ByteArrayInputStream(Arrays.copyOf(icon, icon.length)), new StreamBucket(new ParentBucket(allocator, UNLIMITED_SHARING_STRATEGY, _64_KBPS)));
        InputStream normalInputStream = new ByteArrayInputStream(Arrays.copyOf(icon, icon.length));
        ByteArrayOutputStream throttledOutputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream normalOutputStream = new ByteArrayOutputStream();) {
      ByteStreams.copy(throttledInputStream, throttledOutputStream);
      ByteStreams.copy(normalInputStream, normalOutputStream);
      
      assertEquals(normalOutputStream.toByteArray(), throttledOutputStream.toByteArray());
    }
  }
  
  @Test
  public void should_read_at_expected_speed() throws Exception {
    allocator = fixedBandwidthAllocator(MAX_VALUE);
    
    long elapsedTime = timedCopy(randomInputStream(1000), new StreamBucket(new ParentBucket(allocator, new RoundRobinSharingStrategy(250), _64_KBPS)));
    
    assertThat(elapsedTime, between(3900L, 5000L));
  }
  
  @Test
  public void should_take_longer_when_throttled() throws Exception {
    long startTime = System.currentTimeMillis();
    long normalDuration = 0;
    long multipleThrottledDuration = 0;
    
    try (InputStream normalInputStream = randomInputStream(140 * 1024);
        ByteArrayOutputStream normalOutputStream = new ByteArrayOutputStream();) {
      ByteStreams.copy(normalInputStream, normalOutputStream);
      normalDuration = System.currentTimeMillis() - startTime;
    }
    
    startTime = System.currentTimeMillis();
    allocator = fixedBandwidthAllocator(10_000);
    ParentBucket parentBucket = new ParentBucket(allocator, UNLIMITED_SHARING_STRATEGY, _64_KBPS);
    StreamBucket streamBucket = new StreamBucket(parentBucket);
    long throttledDuration = timedCopy(randomInputStream(140 * 1024), streamBucket);
    
    startTime = System.currentTimeMillis();
    List<Future<Long>> invocations = executor.invokeAll(asList(new TestCallable(new StreamBucket(parentBucket), read140KB()), new TestCallable(new StreamBucket(parentBucket), read140KB()), new TestCallable(new StreamBucket(parentBucket), read140KB()), new TestCallable(new StreamBucket(parentBucket), read140KB()), new TestCallable(new StreamBucket(parentBucket), read140KB())));
    
    for (Future<Long> invocation : invocations) {
      invocation.get();
    }
    multipleThrottledDuration = System.currentTimeMillis() - startTime;
    
    assertThat("Throttled file reading should take longer than normal file reading", throttledDuration, greaterThan(normalDuration));
    assertThat("Throttled multiple file reading should take longer than single file reading", multipleThrottledDuration, greaterThan(throttledDuration));
  }
  
  @Test
  public void should_download_faster_when_bandwidth_is_unlimited() throws Exception {
    BandwidthAllocator limitedAllocator = fixedBandwidthAllocator(32_000);
    StreamBucket throttledTokenBucket = new StreamBucket(new ParentBucket(limitedAllocator, new RoundRobinSharingStrategy(10_000), _64_KBPS));
    BandwidthAllocator unlimitedAllocator = fixedBandwidthAllocator(MAX_VALUE);
    StreamBucket unlimitedTokenBucket = new StreamBucket(new ParentBucket(unlimitedAllocator, UNLIMITED_SHARING_STRATEGY, _64_KBPS));
    
    try {
      long limitedDuration = timedCopy(read140KB(), throttledTokenBucket);
      long unlimitedDuration = timedCopy(read140KB(), unlimitedTokenBucket);
      
      assertThat("Limited speed file reading should take longer than unlimited speed file reading", limitedDuration, greaterThan(unlimitedDuration));
    } finally {
      limitedAllocator.shutdown();
      unlimitedAllocator.shutdown();
    }
  }
  
  @Test
  public void should_read_stream_in_expected_time() throws IOException {
    LocalTime localTimeNow = LocalTime.now();
    String now = localTimeNow.toString();
    String oneMinuteFromNow = localTimeNow.plusMinutes(1).toString();
    
    allocator = new ConfigurableBandwidthAllocator.Builder()
      .resetPeriod(1, SECONDS)
      .addBandwidthDefinition(from(now).to(oneMinuteFromNow).allocate(64))
      .build();
    
    ParentBucket parentBucket = new ParentBucket(allocator, UNLIMITED_SHARING_STRATEGY, 64);
    long timeToRead32Bytes = timedCopy(randomInputStream(32), new StreamBucket(parentBucket));
    long timeToRead96Bytes = timedCopy(randomInputStream(96), new StreamBucket(parentBucket));
    
    assertThat("Stream smaller than allocation should be read in a single period.", timeToRead32Bytes, lessThan(1050L));
    assertThat("Stream smaller than 2 allocations but bigger than 1 should be read across two periods.", timeToRead96Bytes, between(1000L, 2100L));
  }
  
  @Test
  public void should_change_bandwidth_while_reading_a_stream() throws IOException {
    LocalTime localTimeNow = LocalTime.now();
    String now = localTimeNow.toString();
    String fourSecondsFromNow = localTimeNow.plusSeconds(4).toString();
    String tenSecondsFromNow = localTimeNow.plusSeconds(10).toString();
    
    allocator = new ConfigurableBandwidthAllocator.Builder()
      .resetPeriod(1, SECONDS)
      .addBandwidthDefinition(from(now).to(fourSecondsFromNow).allocate(1))
      .addBandwidthDefinition(from(fourSecondsFromNow).to(tenSecondsFromNow).allocate(64))
      .build();
    
    ParentBucket parentBucket = new ParentBucket(allocator, UNLIMITED_SHARING_STRATEGY, 64);
    long timeToRead32Bytes = timedCopy(randomInputStream(64), new StreamBucket(parentBucket));
    
    assertThat("Should take 3 seconds to read first 3 bytes, then 1 second to read the rest", timeToRead32Bytes, between(3000L, 4200L));
  }
  
  @Test
  public void should_use_as_much_bandwidth_as_possible() throws Exception {
    LocalTime localTimeNow = LocalTime.now();
    String now = localTimeNow.toString();
    String fourSecondsFromNow = localTimeNow.plusSeconds(4).toString();
    String tenSecondsFromNow = localTimeNow.plusSeconds(10).toString();
    
    
    allocator = new ConfigurableBandwidthAllocator.Builder()
      .resetPeriod(1, SECONDS)
      .addBandwidthDefinition(from(now).to(fourSecondsFromNow).allocate(64 * 1024))
      .addBandwidthDefinition(from(fourSecondsFromNow).to(tenSecondsFromNow).allocate(1000 * 1024))
      .build();
    
    ParentBucket parentBucket = new ParentBucket(allocator, new RoundRobinSharingStrategy(Integer.MAX_VALUE), Integer.MAX_VALUE);
    
    long startTime = System.currentTimeMillis();
    List<Future<Long>> invocations = executor.invokeAll(asList(new TestCallable(new StreamBucket(parentBucket), randomInputStream(_64_KBPS)), new TestCallable(new StreamBucket(parentBucket), randomInputStream(_64_KBPS)), new TestCallable(new StreamBucket(parentBucket), randomInputStream(_64_KBPS)), new TestCallable(new StreamBucket(parentBucket), randomInputStream(_64_KBPS))));
    
    for (Future<Long> invocation : invocations) {
      invocation.get();
    }
    
    long timeToRead4Streams = System.currentTimeMillis() - startTime;
    
    allocator.shutdown();
    
    assertThat("Should be within 5% of max bandwidth", timeToRead4Streams, lessThanOrEqualTo(4200L));
  }
  
  @AfterMethod(alwaysRun=true)
  public void afterMethod() {
    if (allocator != null) {
      allocator.shutdown();
    }
  }
  
  @AfterClass(alwaysRun=true)
  public void afterClass() {
    executor.shutdownNow();
  }
  
  private InputStream read140KB() throws IOException {
    return randomInputStream(140 * 1024);
  }
  
  private long timedCopy(InputStream inputStream, StreamBucket bucket) throws IOException {
    try (InputStream in = new ThrottledInputStream(inputStream, bucket);
        OutputStream out = new ByteArrayOutputStream()) {
      long startTime = System.currentTimeMillis();
      ByteStreams.copy(in, out);
      return System.currentTimeMillis() - startTime;
    }
  }
  
  private static class TestCallable implements Callable<Long> {
    
    private final StreamBucket tokenBucket;
    private final InputStream inputStream;

    public TestCallable(StreamBucket tokenBucket, InputStream inputStream) {
      this.tokenBucket = tokenBucket;
      this.inputStream = inputStream;
    }

    @Override
    public Long call() throws Exception {
      long startTime = System.currentTimeMillis();
      try (InputStream throttledInputStream = new ThrottledInputStream(inputStream, tokenBucket);
          OutputStream outputStream = new ByteArrayOutputStream();) {
        ByteStreams.copy(throttledInputStream, outputStream);
        
        return System.currentTimeMillis() - startTime;
      }
    }
  }
}
