package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.egis.throttler.ParentBucket;
import com.egis.throttler.StreamBucket;

public class StreamBucketTest {

  @Test
  public void should_return_unused_bytes_to_parent_when_closed() {
    ParentBucket parentBucket = mock(ParentBucket.class);
    StreamBucket bucket = new StreamBucket(parentBucket);
    
    bucket.refill(10);
    bucket.bytesRead(5);
    bucket.close();
    
    verify(parentBucket).refill(5);
  }
}
