package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.egis.throttler.ParentBucket;
import com.egis.throttler.bandwidthallocators.BandwidthAllocator;
import com.egis.throttler.sharingstrategies.SharingStrategy;

public class ParentBucketTest {

  @Test
  public void should_respect_max_bucket_size() {
    SharingStrategy sharingStrategy = mock(SharingStrategy.class);
    
    ParentBucket parentBucket = new ParentBucket(mock(BandwidthAllocator.class), sharingStrategy, 64);
    
    parentBucket.refill(64);
    parentBucket.refillStreamBuckets();
    parentBucket.refill(128);
    parentBucket.refillStreamBuckets();
    
    verify(sharingStrategy, times(2)).allocateBytes(64);
  }
}
