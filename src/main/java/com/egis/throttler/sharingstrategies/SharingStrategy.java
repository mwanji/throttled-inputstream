package com.egis.throttler.sharingstrategies;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.egis.throttler.StreamBucket;


public interface SharingStrategy {
  /**
   * Requests that up to maxBytesToAllocate be allocated to the {@link StreamBucket}.
   * 
   * The exact number of bytes allocated to bucket is up to the SharingStrategy implementation.
   * 
   * @return The number of bytes effectively allocated
   */
  int allocateBytes(int maxBytesToAllocate);
  void add(StreamBucket bucket);
  void remove(StreamBucket bucket);
}
