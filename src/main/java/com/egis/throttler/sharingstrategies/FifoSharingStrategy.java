package com.egis.throttler.sharingstrategies;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javolution.util.FastTable;

import com.egis.throttler.StreamBucket;

public class FifoSharingStrategy implements SharingStrategy {
  
  private final FastTable<StreamBucket> streamBuckets = new FastTable<>();
  private final int maxBytesPerBucket;

  public FifoSharingStrategy(int maxBytesPerBucket) {
    this.maxBytesPerBucket = maxBytesPerBucket;
  }

  @Override
  public int allocateBytes(int maxBytesToAllocate) {
    int allocatedBytes = 0;
    int remainingBytesToAllocate = maxBytesToAllocate;
    for (StreamBucket streamBucket : streamBuckets.atomic()) {
      int allocatable = Math.min(remainingBytesToAllocate, maxBytesPerBucket);
      streamBucket.refill(allocatable);
      allocatedBytes += allocatable;
      remainingBytesToAllocate -= allocatable;
      
      if (remainingBytesToAllocate == 0) {
        return allocatedBytes;
      }
    }
    
    return allocatedBytes;
  }

  @Override
  public void add(StreamBucket bucket) {
    streamBuckets.atomic().add(bucket);
  }

  @Override
  public void remove(StreamBucket bucket) {
    streamBuckets.atomic().remove(bucket);
  }

}
