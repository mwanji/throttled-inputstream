package com.egis.throttler.sharingstrategies;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.egis.throttler.StreamBucket;

import javolution.util.FastTable;

public class RoundRobinSharingStrategy implements SharingStrategy {
  
  private final FastTable<StreamBucket> streamBuckets = new FastTable<>();
  private final int maxBytesPerStreamBucket;

  public RoundRobinSharingStrategy(int maxBytesPerStreamBucket) {
    this.maxBytesPerStreamBucket = maxBytesPerStreamBucket;
  }

  @Override
  public int allocateBytes(int bytes) {
    int allocatedBytes = 0;
    int naturalSharePerBucket = bytes / (Math.max(1, streamBuckets.size()));
    int effectiveBucketShare = Math.min(naturalSharePerBucket, maxBytesPerStreamBucket);
    
    for (StreamBucket bucket : streamBuckets.atomic()) {
      allocatedBytes += effectiveBucketShare;
      bucket.refill(effectiveBucketShare);
    }
    
    return allocatedBytes;
  }

  @Override
  public void add(StreamBucket bucket) {
    streamBuckets.atomic().add(bucket);
  }

  @Override
  public void remove(StreamBucket bucket) {
    streamBuckets.atomic().remove(bucket);
  }
}
