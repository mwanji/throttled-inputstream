package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.concurrent.atomic.AtomicLong;

import com.egis.throttler.bandwidthallocators.BandwidthAllocator;
import com.egis.throttler.sharingstrategies.SharingStrategy;

public class ParentBucket {

  private final AtomicLong bytes = new AtomicLong();
  private final SharingStrategy sharingStrategy;
  private final int byteCeiling;
  
  public ParentBucket(BandwidthAllocator bandwidthAllocator, SharingStrategy sharingStrategy, int byteCeiling) {
    this.sharingStrategy = sharingStrategy;
    this.byteCeiling = byteCeiling;
    bandwidthAllocator.register(this);
  }
  
  public void add(StreamBucket bucket) {
    sharingStrategy.add(bucket);
  }
  
  /**
   * Adds newBytes to the bucket
   */
  public void refill(int newBytes) {
    fillBucket(newBytes);
  }
  
  /**
   * Distributes bucket contents between {@link StreamBucket}s
   */
  public void refillStreamBuckets() {
    int bytesAllocatedToStreamBuckets = sharingStrategy.allocateBytes(bytes.intValue());
    bytes.addAndGet(-bytesAllocatedToStreamBuckets);
  }
  
  public void remove(StreamBucket tokenBucket) {
    sharingStrategy.remove(tokenBucket);
  }
  
  private void fillBucket(int newBytes) {
    bytes.set(Math.min(newBytes, byteCeiling));
  }
}
