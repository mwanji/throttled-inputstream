package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;

public class ThrottledInputStream extends InputStream {
  
  private final InputStream underlyingInputStream;
  private final StreamBucket bucket;

  public ThrottledInputStream(InputStream underlyingInputStream, StreamBucket bucket) {
    this.underlyingInputStream = underlyingInputStream;
    this.bucket = bucket;
  }

  @Override
  public int read() throws IOException {
    bucket.getAvailableBandwidthInBytes();
    int byteRead = underlyingInputStream.read();
    
    bucket.bytesRead(1);
    
    return byteRead;
  }
  
  @Override
  public int read(byte[] b) throws IOException {
    int bytesRead = read(b, 0, b.length);
    
    return bytesRead;
  }
  
  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    int available = Math.min(len, bucket.getAvailableBandwidthInBytes());
    int bytesRead = underlyingInputStream.read(b, off, available);
    
    bucket.bytesRead(bytesRead);
    
    return bytesRead;
  }
  
  /**
   * Closes underlying stream and unregisters from BandwidthManager
   */
  @Override
  public void close() throws IOException {
    bucket.close();
    underlyingInputStream.close();
  }
}
