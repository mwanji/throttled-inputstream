package com.egis.throttler.bandwidthallocators;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.joda.time.LocalTime;

import com.egis.throttler.ParentBucket;

public class ConfigurableBandwidthAllocator implements BandwidthAllocator {

  private final int resetPeriod;
  private final TimeUnit resetTimeUnit;
  private final List<BandwidthDefinition> bandwidthDefinitions;
  private final ScheduledExecutorService threadPool;
  private BandwidthDefinition currentBandwidthDefinition;
  
  @Override
  public void register(final ParentBucket bucket) {
    threadPool.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        bucket.refill(getAvailableBandwidthInBytes());
        bucket.refillStreamBuckets();
      }
    }, 0, resetPeriod, resetTimeUnit);
  }

  @Override
  public void shutdown() {
    threadPool.shutdown();
  }
  
  int getAvailableBandwidthInBytes() {
    LocalTime now = LocalTime.now();
    
    if (currentBandwidthDefinition != null && currentBandwidthDefinition.contains(now)) {
      return currentBandwidthDefinition.bytes;
    }
    
    for (BandwidthDefinition bandwidthDefinition : bandwidthDefinitions) {
      if (bandwidthDefinition.contains(now)) {
        currentBandwidthDefinition = bandwidthDefinition;
        return bandwidthDefinition.bytes;
      }
    }
    
    return 0;
  }
  
  public static class Builder {
    private int resetPeriod;
    private TimeUnit resetTimeUnit;
    private final List<BandwidthDefinition> bandwidthDefinitions = new ArrayList<>();
    
    public Builder resetPeriod(int resetPeriod, TimeUnit resetTimeUnit) {
      this.resetPeriod = resetPeriod;
      this.resetTimeUnit = resetTimeUnit;
      
      return this;
    }
    
    public Builder addBandwidthDefinition(BandwidthDefinition bandwidthDefinition) {
      bandwidthDefinitions.add(bandwidthDefinition);
      
      return this;
    }
    
    public ConfigurableBandwidthAllocator build() {
      return new ConfigurableBandwidthAllocator(resetPeriod, resetTimeUnit, bandwidthDefinitions);
    }
  }
  
  public static class BandwidthDefinition {

    private final LocalTime from;
    private final LocalTime to;
    private final int bytes;

    public static ToBuilder from(String time) {
      return new ToBuilder(LocalTime.parse(time));
    }

    public boolean contains(LocalTime localTime) {
      return (from.isBefore(localTime) || from.isEqual(localTime)) && to.isAfter(localTime);
    }

    public static class ToBuilder {
      private final LocalTime from;

      private ToBuilder(LocalTime from) {
        this.from = from;
      }

      public BytesBuilder to(String time) {
        return new BytesBuilder(from, LocalTime.parse(time));
      }
    }

    public static class BytesBuilder {
      private LocalTime from;
      private final LocalTime to;

      private BytesBuilder(LocalTime from, LocalTime to) {
        this.from = from;
        this.to = to;
      }

      public BandwidthDefinition allocate(int bytes) {
        return new BandwidthDefinition(from, to, bytes);
      }
    }

    private BandwidthDefinition(LocalTime from, LocalTime to, int bytes) {
      this.from = from;
      this.to = to;
      this.bytes = bytes;
    }
  }
  
  private ConfigurableBandwidthAllocator(int resetPeriod, TimeUnit resetTimeUnit, List<BandwidthDefinition> bandwidthDefinitions) {
    this.resetPeriod = resetPeriod;
    this.resetTimeUnit = resetTimeUnit;
    this.bandwidthDefinitions = bandwidthDefinitions;
    this.threadPool = Executors.newScheduledThreadPool(5);
  }
}
