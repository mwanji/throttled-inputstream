package com.egis.throttler.bandwidthallocators;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.joda.time.DateTime;

import com.egis.throttler.ParentBucket;

public class HourlyBandwidthAllocator implements BandwidthAllocator {
  private static final int UNLIMITED = Integer.MAX_VALUE;
  private static final int _1_MBPS = 1_000_000;
  private static final int _64_KBPS = 64_000;

  private final int[] bandwidthPerHour = { 
      UNLIMITED, UNLIMITED, UNLIMITED, UNLIMITED, UNLIMITED, UNLIMITED, UNLIMITED, UNLIMITED,
      _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS, _64_KBPS,
      _1_MBPS, _1_MBPS, _1_MBPS, _1_MBPS, _1_MBPS, _1_MBPS, _1_MBPS
    };
  private final ScheduledExecutorService threadPool;
  
  public HourlyBandwidthAllocator() {
    this(Executors.newScheduledThreadPool(5));
  }
  
  public HourlyBandwidthAllocator(ScheduledExecutorService threadPool) {
    this.threadPool = threadPool;
  }

  public int getAvailableBandwidthInBytes() {
    DateTime now = new DateTime();
    
    return bandwidthPerHour[now.getHourOfDay()];
  }

  @Override
  public void register(final ParentBucket bucket) {
    threadPool.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        bucket.refill(getAvailableBandwidthInBytes());
        bucket.refillStreamBuckets();
      }
    }, 0, 1, SECONDS);
  }

  @Override
  public void shutdown() {
    threadPool.shutdown();
  }
}
