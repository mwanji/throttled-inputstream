package com.egis.throttler;

/*
 * #%L
 * ThrottledInputStream
 * %%
 * Copyright (C) 2014 Egis Software
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Requires {@link #getAvailableBandwidthInBytes()} and {@link #refill(int)} to be called
 * by different threads, as the former blocks on a {@link Semaphore} that the latter releases.
 *
 */
public class StreamBucket {
  public static final int NO_BANDWIDTH_AVAILABLE = 0;
  private final AtomicLong byteTokens = new AtomicLong();
  private final Semaphore lock = new Semaphore(1);
  private ParentBucket parent;
  
  public StreamBucket(ParentBucket parent) {
    this.parent = parent;
    this.parent.add(this);
  }
  
  /**
   * Blocks until some bandwidth is made available.
   */
  public int getAvailableBandwidthInBytes() {
    try {
      lock.acquire();
      int currentByteTokens = byteTokens.intValue();
      if (currentByteTokens <= NO_BANDWIDTH_AVAILABLE) {
        waitForRefill();
      }
      
      return byteTokens.intValue();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    } finally {
      lock.release();
    }
  }
  
  public void close() {
    parent.refill(byteTokens.intValue());
    parent.remove(this);
  }

  public void bytesRead(int bytesRead) {
    if (bytesRead > -1) {
      byteTokens.addAndGet(-bytesRead);
    }
  }

  public void refill(int bytes) {
    byteTokens.set(bytes);
    lock.release();
  }
  
  private void waitForRefill() throws InterruptedException {
    lock.acquire();
  }
}
